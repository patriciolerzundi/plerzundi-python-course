class Persona:

    """Ejemplo *valores es para colocar mas argumentos (tupla)"""
    def __init__(self,nombre,edad, *valores):
        self.nombre = nombre
        self.edad = edad
        self.valores = valores
    
    def desplegar(self):
        print("nombre:", self.nombre)
        print("edad:", self.edad)
        print("Valores (tupla):", self.valores)


# creamos el objeto

p1 = Persona("Patricio",29,56,7,8,9)
p1.desplegar()