from figura_geometrica import FiguraGeometrica
from color import Color

# se toma la clase del lado izquierdo y luego por el derecho
class Cuadrado(FiguraGeometrica,Color):

    def __init__(self,lado,color):
        FiguraGeometrica.__init__(self,lado,lado)
        #super().__init__(lado,lado)
        Color.__init__(self,color)
    
    def area(self):
        return self.alto * self.ancho
       