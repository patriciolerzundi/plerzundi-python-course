#Guarda información de las ordenes de pizza
pizza = {
	'crust':'tink',
	'toppings':['extra queso','champiñones'],
}

print(f"Usted ha ordenado a {pizza['crust']} - crust pizza, con los siguientes ingredientes:")

for topping in pizza['toppings']:
	print(f"\t{topping}")