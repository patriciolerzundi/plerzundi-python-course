from typing import List

class Solution:

    def twoSum(self, nums: List[int],target: int) -> List[int]:

        dictionary = {}
        answer = []

        for  i in range(len(nums)):
            secondNumber = target-nums[i]
            if (secondNumber in dictionary.keys()):
                secondIndex = nums.index(secondNumber)
                if(i!=secondNumber):
                    return sorted([i,secondIndex])
            dictionary.update({nums[i]: i})


if __name__ == "__main__":
    
    # ingresamos valores de entrada
    solution = Solution()
    print(solution.twoSum([3,2,4],6))