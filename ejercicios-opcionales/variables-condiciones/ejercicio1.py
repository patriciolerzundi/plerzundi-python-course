"""
Pedir los coeficientes de una ecuacion de 2° grado, y muestre
las soluciones reales, si no existen debe indicarlo
"""
import math

a=int(input("Ingrese el primer coeficiente:"))
b=int(input("Ingrese el segundo coeficiente:"))
c=int(input("Ingrese el tercer coeficiente:"))

# calculamos el determinante
d = ((b*b)-4*a*c)

if d < 0:
    print("No existen soluciones reales")
else:
    x1=(-b+math.sqrt(d)/(2*a))
    x2=(-b-math.sqrt(d)/(2*a))

    print("x1:",x1)
    print("x2:",x2)




