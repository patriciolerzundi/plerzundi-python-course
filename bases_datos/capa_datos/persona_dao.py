from persona import Persona
from conexion import Conexion
from logger_base import logger


class PersonaDao:
    '''
    DAO (Data Access Object)
    CRUD: Create-Read-Update-Delete entidad Persona
    '''

    __SELECCIONAR = 'SELECT * FROM persona ORDER BY id_persona'
    __INSERTAR = 'INSERT INTO persona(nombre,apellido,email) VALUES(%s,%s,%s)'
    __EDITAR = 'UPDATE persona SET nombre=%s, apellido=%s, email=%s WHERE id_persona=%s'
    __ELIMINAR = 'DELETE FROM persona WHERE id_persona=%s'

    @classmethod
    def seleccionar(cls):
        cursor = Conexion.obtenerCursor()
        logger.debug(cursor.mogrify(cls.__SELECCIONAR))
        cursor.execute(cls.__SELECCIONAR)
        personas = []
        registros = cursor.fetchall()
        for registro in registros:
            persona = Persona(registro[0],registro[1],registro[2],registro[3])
            personas.append(persona)
        Conexion.cerrar()
        return personas 
    
    @classmethod
    def insertar(cls, persona):
        try:
            conexion = Conexion.obtenerConexion()
            cursor = Conexion.obtenerCursor()
            logger.debug(cursor.mogrify(cls.__INSERTAR))
            logger.debug(f'Persona a insertar: {persona}')
            values = (persona.get_nombre(), persona.get_apellido(),persona.get_email())
            cursor.execute(cls.__INSERTAR,values)
            return cursor.rowcount    
        except Exception as e:
            logger.error(f'Excepción al insertar persona: {e}')
            conexion.rollback()
        finally:
            conexion.commit()
            cursor.close()
            conexion.close()

    @classmethod
    def actualizar(cls, persona) -> str:
        try:
            conexion = Conexion.obtenerConexion()
            cursor = Conexion.obtenerCursor()
            logger.debug(cursor.mogrify(cls.__ACTUALIZAR))
            logger.debug(f'Persona a actualizar: {persona}')
            values = (persona.get_nombre(),persona.get_apellido(),persona.get_email(),persona.get_id_persona())
            cursor.execute(cls.__ACTUALIZAR, values)
            return cursor.rowcount
        except Exception as e:
            conexion.rollback()
            logger.error(f'Excepción al actualizar persona {e}')
        finally:
            conexion.commit()
            cursor.close()
            conexion.close()


    @classmethod
    def eliminar(cls,persona):
        try:
            conexion = Conexion.obtenerConexion()
            cursor = Conexion.obtenerCursor()
            logger.debug(f'Persona a eliminar: {persona}')
            values = (persona.get_id_persona(),)
            cursor.execute(cls.__ELIMINAR, values)
        except Exception as e:
            conexion.rollback()
            logger.error(f'Excepción al eliminar persona {e}')
        finally:
            conexion.commit()
            cursor.close()
            conexion.close()


if __name__ == '__main__':
    # personas = PersonaDao.seleccionar()
    # for persona in personas:
    #     logger.debug(persona.get_id_persona())
    #     logger.debug(persona)
    
    persona = Persona(nombre='Pedro',apellido='Najera', email='pnajera@mail.com')
    registros_insetados = PersonaDao.insertar(persona)
    logger.debug(f'Personas insertados: {registros_insetados}')

    # eliminar un registro existente
    # persona = Persona(id_persona=7)
    # personas_eliminadas = PersonaDAO.eliminar(persona)