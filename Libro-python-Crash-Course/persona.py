class Persona:

	contador_persona = 0

	def __init__(self,nombre,apellido,edad):
		Persona.contador_persona += 1
		self.__id = Persona.contador_persona
		self.__nombre = nombre
		self.__apellido = apellido
		self.__edad = edad


	def get_nombre(self) -> str:
		return self.__nombre

	def set_nombre(self,nombre):
		self.__nombre = nombre

	def get_apellido(self) -> str:
		return self.__apellido

	def set_apellido(self,apellido):
		self.__apellido = apellido

	def get_edad(self):
		return str(self.__edad)

	def set_edad(self,edad):
		self.__edad = edad


	def __str__(self):
		return 'Nombre: '+ self.get_nombre() + ', Edad: '+self.get_edad()



if __name__ == '__main__':

	persona1 = Persona('Patricio','Lerzundi',28)
	persona2 = Persona('Juan','Lerzundi',28)
	print(persona1)
	print(persona2)