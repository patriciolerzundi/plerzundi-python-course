class Aritmetica:

    """Clase aritmetica para realizar las operaciones de suma,resta,etc"""
    def __init__(self, operando1,operando2):
        self.operando1 = operando1
        self.operando2 = operando2

    def sumar(self):
        return self.operando1 + self.operando2
    
    def restar(self):
        return self.operando1 - self.operando2
    
    def multiplicar(self):
        return self.operando1 * self.operando2

#Creamos un nuevo objeto
aritmetica = Aritmetica(3,5)
print("La suma es:{0} ".format(aritmetica.sumar()))