import psycopg2 as db

conexion = db.connect(user="admin",
                 password="password",
                 host="192.168.159.137",
                 port="5432",
                 database="test_db")


#sentencia = 'SELECT * FROM persona WHERE id_persona = %s'
try:
    cursor = conexion.cursor()
    sentencia = 'SELECT * FROM persona WHERE id_persona IN %s'
#id_persona = 1
#id_persona = int(input("Proporciona la pk a buscar: "))
    entrada = input("Proporciona las pk a buscar (separado por comas): ")
    tupla  = tuple(entrada.split(',')) # separa por comas
#llave_primaria = (id_persona,)
    llave_primarias = (tupla,)
    cursor.execute(sentencia,llave_primarias)
#registro = cursor.fetchone()
    registros = cursor.fetchall()
    for registro in registros:
        print(registro)    

except Exception as e:
    print(e)

finally:
# cierre de las conexiones
    cursor.close()
    conexion.close()
