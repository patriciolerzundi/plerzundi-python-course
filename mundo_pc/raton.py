from dispositivo_entrada import DispositivoEntrada

class Raton(DispositivoEntrada):

    # contador id ratones
    contador_ratones = 0

    def __init__(self,marca,tipo_entrada):
        Raton.contador_ratones+=1
        self.__id_raton = Raton.contador_ratones

        #Accedemos a los atributos protegidos
        self.__marca = marca
        self.__tipo_entrada = tipo_entrada

    def __str__(self):
        return (f"Id: {self.__id_raton}, "
                f"marca: {self.__marca}, "
                f"tipo_entrada: {self.__tipo_entrada}")


# raton = Raton("hp","usb")
# print(raton)