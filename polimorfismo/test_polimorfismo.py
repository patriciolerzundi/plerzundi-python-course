from empleado import Empleado
from gerente import Gerente


def imprimir_detalles(objeto):
    print(objeto)  # __str__() polimorfismo de la clase padre
    print(type(objeto), end="\n\n")
    if isinstance(objeto, Gerente):
        print(objeto.departamento)


empleado = Empleado("Patrico", 1000.00)
imprimir_detalles(empleado)

empleado = Gerente("Kymber", 2000.00, "Gerencia")
imprimir_detalles(empleado)
