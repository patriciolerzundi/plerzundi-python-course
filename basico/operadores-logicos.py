a = 3
valorMinimo = 0
valorMaximo = 5

dentroRango = ( a >= valorMinimo and a <= valorMaximo)
#print(dentroRango)
if(dentroRango): # por defecto el if viene como true
    print("Adentro del rango")
else:
    print("Fuera del rango")

vacaciones = False
diaDescanso = True

if(vacaciones or diaDescanso):
    print("Puedes ir al parque")
else:
    print("Tienes trabajo que hacer")

print(not(vacaciones)) # el operador not devuelve el valor opuesto booleano

if not(vacaciones or diaDescanso):
    print("no tienes vacaciones")
else:
    print("Si tienes vacaciones")