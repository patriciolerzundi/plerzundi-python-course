"""
El objetivo del ejercicio es crear un sistema de calificaciones, como sigue:
El usuario proporcionará un valor entre 0 y 10.
"""

calificacion = int(input("Ingrese un valor entre 0 y 10:"))

if calificacion > 10:
    print("Fuera de rango, debes proporcionar un valor de 0 a 10")
elif (calificacion == 9 or calificacion == 10):
    print("A")
elif calificacion < 9 and calificacion >= 8:
    print("B")
elif calificacion < 8 and calificacion >= 7:
    print("C")
elif calificacion < 7 and calificacion >= 6:
    print("D")
elif calificacion < 6 and calificacion >=0:
    print("F")
