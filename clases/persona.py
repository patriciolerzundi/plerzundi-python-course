class Persona:
    
    # es como un constructor
    def __init__(self, nombre,edad):
        self.nombre = nombre
        self.edad = edad
        

#Modificar los valores
Persona.nombre = "Patricio"
Persona.edad = 29

#Acceder los valores
print(Persona.nombre)
print(Persona.edad)

#Creación de un objeto, es como una instancia
persona = Persona("Kymber",26)
print(persona.nombre)
print(persona.edad)
print(id(persona)) # muestra direccion en memoria

#Creación de un objeto, es como una instancia
persona2 = Persona("Kymberlee",26)
print(persona2.nombre)
print(persona2.edad)
print(id(persona2)) # muestra direccion en memoria