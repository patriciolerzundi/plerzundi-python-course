import psycopg2 as db

conexion = db.connect(user="admin",
                 password="password",
                 host="192.168.159.137",
                 port="5432",
                 database="test_db")

try:

    cursor = conexion.cursor()
    sentencia = 'INSERT INTO persona(nombre,apellido,email) VALUES(%s,%s,%s)'
    valores = ('Jonathan','Avendaño','javendaño@gmail.com')
    cursor.execute(sentencia,valores)

    registros_insertados = cursor.rowcount
    print(f'Registros insertados: {registros_insertados}')

except Exception as e:
    conexion.rollback()
    print(e)

finally:
    conexion.commit()
    cursor.close()
    conexion.close()