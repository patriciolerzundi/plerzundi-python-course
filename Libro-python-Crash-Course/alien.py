alien_0 = {'x_position':0,'y_position':25,'speed':'medium','points':5}
print(f"posicion original: {alien_0['x_position']}")


# mover al alien a la derecha
# determinar cuanto se mueve el alien
if alien_0['speed'] == 'low':
	x_increment = 1
elif alien_0['speed'] == 'medium':
	x_increment = 2
else:
	x_increment = 3

#incrementamos la posicion antigua con la nueva
alien_0['x_position']+= x_increment

print(f"nueva posicion {alien_0['x_position']}")
# eliminamos los puntos
del alien_0['points']
print(alien_0)

# aliens_0 = {'color':'green','points':5}

# print(aliens_0['color'])
# print(aliens_0['points'])
# print(aliens_0)

# new_points = aliens_0['points']
# print(f'has ganado {new_points} puntos, felicidades')

# aliens_0['x_position'] = 0
# aliens_0['y-position'] = 25
# print(aliens_0)

# aliens_0 = {'color':'green'}
# print(f'El alien es: {aliens_0["color"]}.')

# aliens_0['color'] = 'red'
# print(f"El alien es: {aliens_0['color']}")

# print(aliens_0)