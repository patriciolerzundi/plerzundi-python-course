# Tupla mantiene el orden, pero ya no se puede modificar
frutas = ("Naranja","Platanos","Guayaba","Almedra")
print(frutas)
#largo de la tupla
print(len(frutas))
#accediendo a un elemento
print(frutas[2])
#navegacion inversa
print(frutas[-1])
#rango
print(frutas[0:2])
#Tupla al reves
print(frutas[::-1])

# transformar una tupla a lista
frutasLista = list(frutas)
frutasLista[1] = "Platanito"
frutas = tuple(frutasLista)
print(frutas)

for fruta in frutas:
    print(fruta, end="|")