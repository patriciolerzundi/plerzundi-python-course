import psycopg2 as db

conexion = db.connect(user="admin",
                 password="password",
                 host="192.168.159.137",
                 port="5432",
                 database="test_db")

try:

    cursor = conexion.cursor()
    sentencia = 'UPDATE persona SET nombre = %s, apellido = %s, email = %s WHERE id_persona = %s'
    valores = ('Kymberlee','Roth','conejitarainbow@gmail.com',2)
    cursor.execute(sentencia,valores)

    # registros actualizados
    registros_actualizados = cursor.rowcount
    print(f'Registros actualizados: {registros_actualizados}')

except Exception as e:
    print(e)

finally:

    conexion.commit()
    cursor.close()
    conexion.close()