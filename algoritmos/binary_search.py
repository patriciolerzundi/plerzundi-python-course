"""

Suponga que está buscando a una persona en la guía telefónica (¡qué frase tan anticuada!).
Su nombre comienza con K. Podrías comenzar por el principio y seguir pasando las páginas hasta llegar a las K.
Pero es más probable que comience en una página en el medio, porque sabe que las K estarán cerca del medio de la guía telefónica.

"""
def binary_search(list, item):
    low = 0
    high = len(list) - 1

    while low <= high:
        mid = (low + high) // 2
        guess = list[mid]
        if guess == item:
            return mid
        if guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return None


my_list = [1, 2, 3, 4, 5, 6, 7]

print(binary_search(my_list, 3))
