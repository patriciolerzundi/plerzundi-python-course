import os
from pyspark.sql import SparkSession
from pyspark.sql.functions import split, first, col, explode, expr

from pyspark.sql.types import  (
    StructType,
    StructField,
    StringType,
    IntegerType,
    TimestampType
)

spark =  SparkSession.builder.master("local[*]").getOrCreate()

def ruta(dataset_name):
    main_path = '/home/jovyan/work/datasets/'
    full_path = os.path.join(main_path,dataset_name)
    return full_path

ruta_dataset = ruta('log.csv')
print(ruta_dataset)

trx_schema = StructType(
    [ 
        StructField("ID",StringType(),False),
        StructField("LOG_FECHA_REGISTRO",TimestampType(),False),
        StructField("LOG_TRX_SERVICIO_CODIGO",StringType(),False),
        StructField("LOG_ADQUIRENTE_CODIGO",StringType(),False),
        StructField("LOG_EMISOR_CODIGO",StringType(),False),
        StructField("LOG_SUCURSAL_CODIGO",StringType(),False),
        StructField("LOG_TERMINAL_CODIGO",StringType(),False),
        StructField("LOG_CAJA_NUMERO",StringType(),False),
        StructField("LOG_CAJA_FECHA",TimestampType(),False),
        StructField("LOG_CAJA_NRO_OPERACION",StringType(),False),
        StructField("LOG_CAJA_NRO_SECUENCIA",StringType(),False),
        StructField("LOG_TRX_MONTO",StringType(),False),
        StructField("LOG_MENSAJERIA_CODIGO",StringType(),False),
        StructField("LOG_INSTRUMENTO_CODIGO",StringType(),False),
        StructField("LOG_TRX_RETORNO_TXC",StringType(),False),
        StructField("LOG_TRX_RETORNO_TXC_GLOSA",StringType(),False),
        StructField("LOG_TRX_RETORNO_AUT",StringType(),False),
        StructField("LOG_TRX_RETORNO_AUT_GLOSA",StringType(),False),
        StructField("LOG_TRX_CODIGO_AUT",StringType(),False),
        StructField("LOG_TRX_ESTADOS",StringType(),False),
        StructField("LOG_TRX_REQUERIMIENTO_DATA",StringType(),False),
        StructField("LOG_TRX_RESPUESTA_DATA",StringType(),False),
        StructField("LOG_ID_TRX_ORIGINAL",StringType(),False),
        StructField("LOG_NUM_TARJETA",StringType(),False),
        StructField("LOG_NUM_CUENTA",StringType(),False),
        StructField("LOG_FECHA_CONTABLE",TimestampType(),False)   
    ]
)

df = spark.read.csv(ruta_dataset,header=True,inferSchema=True,sep='|',schema=trx_schema)

df_fraud = df.alias("df_fraud")
df_fraud = df_fraud.orderBy("ID")

df.printSchema()

# Dividir el campo trx_data en pares llave-valor
df = df.withColumn("key_value_pairs", split(df["LOG_TRX_REQUERIMIENTO_DATA"], "&"))

# Explotar los pares llave-valor en filas separadas
df = df.select("LOG_TRX_REQUERIMIENTO_DATA", explode(df["key_value_pairs"]).alias("key_value"))

# Separar los pares llave-valor en columnas de llave y valor
df = df.withColumn("key", split(df["key_value"], "=").getItem(0))
df = df.withColumn("value", split(df["key_value"], "=").getItem(1))

# Pivote para transformar los datos en un formato estructurado
pivot_df = df.groupBy("LOG_TRX_REQUERIMIENTO_DATA").pivot("key").agg(expr("coalesce(first(value), '')"))

for col in pivot_df.columns:
    if col != "null":
        pivot_df = pivot_df.withColumnRenamed(col, col.replace(" ", "_"))

campos_no_necesario = ["LOG_TRX_REQUERIMIENTO_DATA"]
pivot_df= pivot_df.drop(*campos_no_necesario)

campos_no_necesario_fraud = ["LOG_TRX_REQUERIMIENTO_DATA","LOG_TRX_RESPUESTA_DATA"]
df_fraud = df_fraud.drop(*campos_no_necesario_fraud)

df_fraud = df_fraud.withColumn("ID", df_fraud["ID"].cast(IntegerType()))
pivot_df = pivot_df.withColumn("LOG_ID", pivot_df["LOG_ID"].cast(IntegerType()))

result_df = df_fraud.join(pivot_df, pivot_df["LOG_ID"] == df_fraud.ID, "left")

result_df.write.option("sep","|").csv('/home/jovyan/work/datasets/output', header=True, mode="overwrite")