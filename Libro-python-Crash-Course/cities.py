prompt = "\nPor favor ingresar las ciudades que has visitado. "
prompt+= "\n Presiona Enter 'quit', cuando hayas finalizado. "

while True:
    city = input(prompt)

    if city == 'quit':
        break
    else:
        print(f"Yo amo al país: {city.title()}")