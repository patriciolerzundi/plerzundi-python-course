class Vehiculo:
    
    def __init__(self,color,ruedas):
        self.__color = color;
        self.__ruedas = ruedas
    
    def get_color(self):
        return self.__color

    def set_color(self,color):
        self.__color = color

    def get_ruedas(self):
        return self.__ruedas
    
    def set_ruedas(self,ruedas):
        self.__ruedas = ruedas

    def __str__(self):
        return "Color:"+ self.get_color() + ", Ruedas:" + str(self.get_ruedas())

class Coche(Vehiculo):
    
    def __init__(self, color, ruedas,velocidad):
        super().__init__(color, ruedas)
        self.__velocidad = velocidad
    
    def get_velocidad(self):
        return self.__velocidad
    
    def set_velocidad(self,velocidad):
        self.__velocidad = velocidad
        
    
    def __str__(self):
        return super().__str__() + ", Velocidad (km/h): "+ str(self.get_velocidad())

class Bicicleta(Vehiculo):

    def __init__(self, color, ruedas,tipo):
        super().__init__(color, ruedas)
        self.__tipo = tipo

    
    def get_tipo(self):
        return self.__tipo

    def set_tipo(self,tipo):
        self.__tipo = tipo

    def __str__(self):
        return super().__str__()+ ", Tipo (urbana/montaña/etc):" + self.get_tipo()

vehiculo = Vehiculo("azul",4)
print(vehiculo)
coche1 = Coche("rojo",4,20)
print(coche1)
bici_montania = Bicicleta("Amarilla",2,"Montaña")
print(bici_montania)