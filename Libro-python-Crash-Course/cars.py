cars = ['bmw','audi','toyota','subaru']
# cars.sort()
# cars.sort(reverse=True)
print(f'Lista original {cars}')
print(f'Lista temporal {sorted(cars)}')

cars.reverse()
print(f'Lista en reversa {cars}')
