from Producto import Producto
from Orden import Orden

producto1 = Producto("Camisa",100.10)
producto2 = Producto("Pantalón",200.00)
producto3 = Producto("Calcetines",70.00)

productos = [producto1,producto2]
#print(productos)

orden1 = Orden(productos)
print(orden1)

# se agrega el último elemento
# Segunda orden de productos
orden2 = Orden(productos)
orden2.agregar_producto(producto3)
print(orden2)
print("Total de la Orden: ",orden2.calcular_total())
