class Persona:

    def __init__(self,nombre,ape_paterno,ape_materno):
        self.nombre = nombre
        self.__ape_paterno = ape_paterno
        self.__ape_materno = ape_materno
    
    def metodo_publico(self):
        self.__metodo_privado()

    # este método es privado
    def __metodo_privado(self):
        print(self.nombre)
        print(self.__ape_paterno)
        print(self.__ape_materno)

    def get_apellido_paterno(self):
        return self.__ape_paterno

p1 = Persona("Patricio","Lerzundi","Suazo")
#p1.metodo_publico()
print(p1.get_apellido_paterno())