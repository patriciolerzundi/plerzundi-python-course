users ={
	'lerzundi':{
		'first':'patricio',
		'last':'lerzundi',
		'location':'santiago',
	},

	'rodriguez':{
		'first':'sebastian',
		'last':'rodriguez',
		'location':'santiago'
	},
}


for username, user_info in users.items():
	print(f'\nUsername:{username}')
	full_name = f"{user_info['first']} {user_info['last']}"
	location = user_info['location']

	print(f'\tFullname: {full_name.title()}')
	print(f'\tLocation: {location.title()}') 