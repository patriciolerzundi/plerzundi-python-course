from numeros_identicos_exception import NumerosIdenticosException

resultado = None

try:
    a = int(input("Primer número: "))
    b = int(input("Segundo número: "))
    if a == b:
        raise NumerosIdenticosException("Ocurrió un error, números idénticos")
    resultado = a / b
except ZeroDivisionError as e: #<= es más especifico
#except Exception as e: # <= más generico
    print("Ocurrió un error.",e)
    print(type(e))

except TypeError as e:
    print("Ocurrió un error",e)
    print(type(e))

# except ValueError as e:
#     print("Ocurrió un error con ValueError",e)
#     print(type(e))

except Exception as e:
    print("Ocurrió un error",e)
    print(type(e))

else:
    print("No hubo ninguna excepción")

finally:
    print("Fin del manejo de la excepciones")

print("resultado", resultado)
print("continuamos...")