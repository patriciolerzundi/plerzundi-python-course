from orden import Orden
from computadora import Computadora
from monitor import Monitor
from raton import Raton
from teclado import  Teclado

teclado_hp = Teclado("hp","usb")
raton_hp = Raton("hp","usb")
monitor_hp = Monitor("hp","15 pulgadas")
computadora_hp = Computadora("hp",monitor_hp,teclado_hp,raton_hp)


teclado_acer = Teclado("acer","usb")
raton_acer = Raton("acer","usb")
monitor_acer = Monitor("acer","15 pulgadas")
computadora_acer = Computadora("acer",monitor_acer,teclado_acer,raton_acer)

computadoras_1 = [computadora_hp,computadora_acer]

orden1 = Orden(computadoras_1)
print(orden1)
