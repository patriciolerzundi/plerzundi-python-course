favorite_language = {
	'patricio':['C++','Python3','Java'],
	'cristian':['Bash'],
	'sebastian':['Javascript','java'],
}

for name,languages in favorite_language.items():
	print(f"\n{name.title()} sus lenguajes favoritos son:")
	for language in languages:
		print(f"\t{language.title()}")