class Geometria:

    def __init__(self,base,altura):
        self.base = base
        self.altura = altura
    
    def cuadrado_area(self):
        return self.base * self.altura


class Caja:

    def __init__(self,largo,ancho,alto):
        self.largo = largo
        self.ancho = ancho
        self.alto = alto
    
    def calcular_volumen(self):
        return self.largo* self.ancho * self.alto

# creamos el objeto

area_rectangulo = Geometria(5,5)
print("A={0}".format(area_rectangulo.cuadrado_area()))
print(id(area_rectangulo))

volumen_caja = Caja(3,5,6)
print("{0}m^3".format(volumen_caja.calcular_volumen()))
print(id(volumen_caja))
        