from dispositivo_entrada import DispositivoEntrada

class Teclado(DispositivoEntrada):

    contador_teclado = 0


    def __init__(self,marca,tipo_entrada):

        # contador por id del objeto creado
        Teclado.contador_teclado +=1
        self.__id_teclado = Teclado.contador_teclado
        self.__marca = marca
        self.__tipo_entrada = tipo_entrada

    def __str__(self):
        return(
            f"Id: {self.__id_teclado}, "
            f"marca: {self.__marca}, "
            f"tipo_entrada: {self.__tipo_entrada}"
        )


# teclado = Teclado("hp","bluetooth")
# print(teclado)