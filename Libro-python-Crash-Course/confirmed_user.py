unconfirmed_users = ['patricio','sebastian','cristian']
confirmed_users = []

while unconfirmed_users:
    current_users = unconfirmed_users.pop()
    print(f"Verificando usuario: {current_users.title()}")
    confirmed_users.append(current_users)

print(f"\nLos siguientes usuarios han sido confirmados:")
for confirmed_user in confirmed_users:
    print(confirmed_user.title())
