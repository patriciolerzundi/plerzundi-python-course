import psycopg2 as db

conexion = db.connect(user="admin",
                 password="password",
                 host="192.168.159.137",
                 port="5432",
                 database="test_db")

try:

    cursor = conexion.cursor()
    sentencia = 'DELETE FROM persona WHERE id_persona = %s'
    entrada = input('Proporciona la pk a eliminar: ')
    valores = (entrada,)
    cursor.execute(sentencia,valores)

    # registros actualizados
    registros_eliminados = cursor.rowcount
    print(f'Registros eliminados: {registros_eliminados}')

except Exception as e:
    print(e)

finally:

    conexion.commit()
    cursor.close()
    conexion.close()