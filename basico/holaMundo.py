import math



print("Calcular el area de un circulo:")
entrada = float(input("Ingrese el número (r) para calcular el área \n"))

# el más óptimo para traer resultado de dos decimales
area = (math.pi * (entrada)**2)
print("el area es {0:.2f}".format(area))


print("Antiguo\n",id(area)) # en donde se almacena en memoria
nuevaPosicion = area
print("Nuevo\n",id(nuevaPosicion))
print("Tipo:\n",type(nuevaPosicion))

