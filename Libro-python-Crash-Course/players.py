players = ['patricio','juan','kymber','roberto','felipe','esteban','rodrigo','gonzalo']
print(players[0:3])
print(players[:4])
print(players[4:])
print(players[-1])
print(players[:-4])
print(players[-4:])

# se copia el listado original
players2 = players[:]
print(players2)