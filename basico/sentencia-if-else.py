# sentencia if clasica
#if True:
#    print("Es verdadero")
#else:
#    print("Es falso")


# sentencia if simplificada

#print("Condicion VERDADERA") if True else print("Condicion FALSA")


entrada = input("Ingrese un valor")


if(type(entrada) != int):
    print("Es otro valor")
else:
    print("Es un entero")