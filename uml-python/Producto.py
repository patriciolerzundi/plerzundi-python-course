class Producto:

    contador_producto = 0

    # Contructor Producto

    def __init__(self,nombre,precio):
        Producto.contador_producto+=1
        self.__id_producto=Producto.contador_producto
        self.__nombre = nombre
        self.__precio = precio
    
    
    def get_precio(self):
        return self.__precio
    
    def set_precio(self,precio):
        self.__precio = precio
    
    # To String Objeto Producto
    def __str__(self):
        return "Id Producto:" + str(self.__id_producto) + ", Nombre:"+ self.__nombre + ", Precio:" + str(self.__precio)

