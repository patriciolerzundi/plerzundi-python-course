from figura_geometrica import FiguraGeometrica
from color import Color

class Rectangulo(FiguraGeometrica,Color):
    
    def __init__(self,base,altura,color):
        FiguraGeometrica.__init__(self, base, altura)
        Color.__init__(self,color)
    
    def area(self):
        return self.ancho * self.alto