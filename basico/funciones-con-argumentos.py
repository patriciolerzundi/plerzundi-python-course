#Parametro es una variable definida entre los parentesis de la funcion
#Argumento (arg) es un valor enviado a la función

def funcion_arg(nombre,apellido):
    print("El nombre recibido es:", nombre)
    print("El apellido recibido es:",apellido)

funcion_arg("Patricio","Lerzundi")

def suma(a=0,b=0):
    return a + b

print(suma(5,10))