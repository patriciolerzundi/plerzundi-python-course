nombres = ["Juan","Patricio","Fernando","Kymber","Erika"]

for nombre in nombres:
    if nombre == "Kymber":
        continue
    print(nombre)

print(len(nombres))
print(nombres[0])
print(nombres[-1])
print(nombres[-2])
# imprimir rango
print(nombres[0:2])
print(nombres[:3])# sin incluir el indice 3
print(nombres[1:])
#Cambiar los elementos de una lista
nombres[3] = "Ivone"
print(nombres[::-1])

#revisar si existe un elemento de la lista
if "Patricio" in nombres:
    print("Si existe Juan en la lista")
else:
    print("el elemento buscado no coincide con la lista")


# Agregar elemento a la lista
nombres.append("Cristian")
# insertar un nueveo elemento con el indice proporcionado
nombres.insert(1,"Pancho")
# Remover elemento de una lista
nombres.remove("Patricio")
#remover el ultimo de nuestra lista
nombres.pop()
#remover el indice indicado de la lista
del nombres[0]
#limpiar los elementos de nuestra lista
nombres.clear()
print(nombres)