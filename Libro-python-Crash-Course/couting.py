# current_number = 1

# while current_number <= 5:
#     print(f"N = {current_number}")
#     current_number += 1

current_number = 0

while current_number < 10:
    current_number += 1
    # se salta los numeros par
    if current_number % 2==0:
        continue
    print(current_number)
    