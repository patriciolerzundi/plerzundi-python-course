import logging

logger = logging

logger.basicConfig(level= logging.DEBUG,
                    format='%(asctime)s: %(levelname)s [%(filename)s:%(lineno)s] %(message)s ',
                    datefmt='%I:%M:%S %p',
                    handlers=[
                        logging.FileHandler('capa_datos.log',encoding='utf-8'),
                        logging.StreamHandler()
                    ])

# siempre y cuando se ejecute de este archivo
if __name__ == '__main__':
    logging.warning('mensaje a nivel de warning')
    logging.info('mensaje a nivel de info')
    logging.debug('mensaje a nivel debug')
    logging.error('Ocurrió un error en la bases de datos')
    logging.debug('se realizo la conexión con éxito')