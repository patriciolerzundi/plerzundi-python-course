"""
r -> Read
a -> Append
w -> Write
x -> Create

# handled binary or text mode

t -> Text-Default value. Text mode
b -> Binary mode (e.g images)

"""

try:

    archivo = open("prueba.txt","w", encoding='utf-8')
    archivo.write("Agregamos info al archivo\n")
    archivo.write("Adiós")
except Exception as e:
    print(e)

finally:
    archivo.close()