import psycopg2 as db

conexion = db.connect(user="admin",
                 password="password",
                 host="192.168.159.137",
                 port="5432",
                 database="test_db")



try:

    cursor = conexion.cursor()
    sentencia = 'INSERT INTO persona(nombre, apellido, email) VALUES(%s,%s,%s)'
    valores = (
        ('Raul','Gato','rato@gmail.com'),
        ('Angel','Quintana','aquintana@gmail.com'),
        ('Bombon','Utonio','butonio@gmail.com')       
    )

    cursor.executemany(sentencia,valores)
    
    # verificar cuantos registros se insetaron 
    registros_insertados = cursor.rowcount
    print(f'Registros insertados: {registros_insertados}')

except Exception as e:
    print(e)

finally:

    conexion.commit()
    cursor.close()
    conexion.close()