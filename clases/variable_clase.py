class miClase:

    variable_clase = "Variable de clase"

    def __init__(self,variable_instancia):
        self.variable_instancia = variable_instancia


print(miClase.variable_clase)
objeto1 = miClase("Variable de Instancia")
miClase.variable_instancia = "Modificando variable de instancia"
print(miClase.variable_instancia) #valores distintos
print(objeto1.variable_instancia) #valores distintos

#Podemos acceder a las variables de clase desde los objetos
print(objeto1.variable_clase)
#Podemos acceder a las variables con el nombre de la clase
print(miClase.variable_clase)

objeto1.variable_clase = "Modificando cariable de clase"
print(objeto1.variable_clase)
print(miClase.variable_clase)

objeto2 = miClase("Nuevo valor de la vaariable de instancia")
print(objeto2.variable_clase)

miClase.variable_clase = "Cambio desde la clase"

objeto3 = miClase("tercer objeto")