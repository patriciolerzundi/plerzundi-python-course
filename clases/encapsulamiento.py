class Persona:

    def __init__(self,nombre):
        self.__nombre = nombre # atributo privado __
        self.__edad = 18

    def get_nombre(self):
      return self.__nombre
    
    def set_nombre(self,nombre):
        self.__nombre = nombre
    
    def get_edad(self):
        return self.__edad
    
    def set_edad(self,edad):
        self.__edad = edad

p1 = Persona("Patricio")
print(p1.get_nombre())
print(p1.get_edad())