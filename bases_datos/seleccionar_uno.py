import psycopg2 as db

conexion = db.connect(user="admin",
                 password="password",
                 host="192.168.159.137",
                 port="5432",
                 database="test_db")


cursor = conexion.cursor()
sentencia = 'SELECT * FROM persona WHERE id_persona = %s'
#id_persona = 1
id_persona = int(input("Proporciona la pk a buscar: "))
llave_primaria = (id_persona,)
cursor.execute(sentencia,llave_primaria)
registro = cursor.fetchone()
print(registro)

cursor.close()
conexion.close()
