archivo = open("prueba.txt","r",encoding='utf-8')
#print(archivo.read())

#leer algunos caracteres
#print(archivo.read(5))
#sirve para leer toda la línea
#print(archivo.readline())

# iterando

# for linea in archivo:
#     print(linea)

# leer lineas
# print(archivo.readlines())

# print(archivo.readlines()[0])


# archivo2 = open("copia.txt", "w+",encoding='utf-8') -> de escritura, pero si no queremos
# reemplazar lo existente usuar a de append
archivo2 = open("copia.txt","a",encoding="utf-8")
archivo2.write(archivo.read())



archivo.close()
archivo2.close()