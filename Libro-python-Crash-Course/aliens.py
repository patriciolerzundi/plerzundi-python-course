# alien_0 = {'color':'green','points':5}
# alien_1 = {'color':'red','points': 10}
# alien_2 = {'color':'blue','points':20}

# aliens = [alien_0,alien_1,alien_2]

# for alien in aliens:
# 	print(alien)

aliens = []

for alien_number in range(30):
	new_alien = {'color':'green','points':5,'speed':'low'}
	aliens.append(new_alien)


# modifica los primeros 3 aliens, cambiando las estadisticas
for alien in aliens[:3]:
	if alien['color'] == 'green':
		alien['color'] = 'yellow'
		alien['points'] = 10
		alien['speed'] = 'medium'
	elif alien['color'] == 'yellow':
		alien['color'] = 'red'
		alien['points'] = 15
		alien['speed'] = 'high'


for alien in aliens[:5]:
	print(alien)
print("...")

print(f'total de aliens creados: {len(aliens)}')